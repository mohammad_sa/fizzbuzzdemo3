﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FizzBuzz.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FizzBuzz.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        public HomeController(IConfiguration iConfig)
        {
            _configuration = iConfig;
        }
        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Index(IndexModel indexModel, int currentPageIndex = 1)
        {
            int maxRows = 20;
            string endpoint = string.Empty;
            if (ModelState.IsValid)
            {
                endpoint = _configuration.GetValue<string>("WebAPIBaseUrl") + ((int)indexModel.InputData);
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync(endpoint))
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            IList<string> lstMessages = JsonConvert.DeserializeObject<List<string>>(apiResponse);

                            IndexModel result = new IndexModel
                            {
                                Messages = lstMessages.Skip((currentPageIndex - 1) * maxRows).Take(maxRows).ToList(),
                                PageCount = (int)Math.Ceiling((double)(lstMessages.Count() / Convert.ToDecimal(maxRows))),
                                CurrentPageIndex = currentPageIndex,
                                InputData = indexModel.InputData
                            };
                            return View(result);
                        }
                        else
                        {
                            ModelState.Clear();
                            ModelState.AddModelError(string.Empty, "Service is down. Please try after some time");
                            return View();
                        }
                    }
                }
            }
            else
            {
                return View();
            }
        }
    }
}