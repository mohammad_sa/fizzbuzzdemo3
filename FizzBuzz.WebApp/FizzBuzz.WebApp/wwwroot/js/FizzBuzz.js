﻿$(document).ready(function () {
    var ul = document.getElementById("ulMessage");
    if (ul != null) {
        var items = ul.getElementsByTagName("li");
        for (var count = 0; count < items.length; count++) {
            if (items[count].innerText == "Fizz" || items[count].innerText == "Wizz") {
                items[count].style.color = "blue";
            }
            else if (items[count].innerText == "Buzz" || items[count].innerText == "Wuzz") {
                items[count].style.color = "green";
            }
        }
    }
});

function PagerClick(index) {
    document.getElementById("hfCurrentPageIndex").value = index;
    document.forms[0].submit();
}