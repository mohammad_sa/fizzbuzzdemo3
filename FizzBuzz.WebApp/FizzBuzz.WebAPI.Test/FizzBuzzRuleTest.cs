﻿using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.WebAPI.Test
{

    public class FizzBuzzRuleTest
    {
        [Fact]
        public void TestFizzBuzzRule_IsNumberMatched_WhenNumberDivisibleBy3And5()
        {
            //Arrange
            var result = new FizzBuzzRule();

            //Act
            bool actualResult = result.IsNumberMatched(30);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestFizzBuzzRule_IsNumberMatched_WhenNumberNotDivisibleBy3And5()
        {
            //Arrange
            var result = new FizzBuzzRule();

            //Act
            bool actualResult = result.IsNumberMatched(10);

            //Assert
            Assert.False(actualResult);
        }

        [Theory]
        [InlineData("Wednesday", "Wizz Wuzz")]
        [InlineData("Monday", "Fizz Buzz")]
        [InlineData("Tuesday", "Fizz Buzz")]
        public void TestFizzBuzzRule_GetReplacedWord(string inputValue, string expectedValue)
        {
            //Arrange
            var result = new FizzBuzzRule();
            //Act
            var actualResult = result.GetReplacedWord(inputValue);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }

    }
}
