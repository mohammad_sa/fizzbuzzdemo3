﻿using FizzBuzz.WebAPIService.BusinessLayer.CreateText;
using FizzBuzz.WebAPIService.BusinessLogic;
using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.WebAPI.Test
{
    public class CreateTextServiceTest
    {
        private readonly IEnumerable<IRule> rules;
        public CreateTextServiceTest()
        {
            rules = new List<IRule>
            {
                new FizzRule(),
                new BuzzRule(),
                new FizzBuzzRule()
            };
        }
        [Theory, MemberData(nameof(InputAndExpectedData))]
        public void Test_GenerateFizzBuzzMethod(int inputValue1, string inputValue2, List<string> expectedValue)
        {
            //Arrange            
            var result = new CreateTextService(rules);

            //Act
            IList<string> actualResult = result.GetMessages(inputValue1, inputValue2);

            //Assert
            Equals(expectedValue, actualResult);
        }

        public static IEnumerable<object[]> InputAndExpectedData =>
           new List<object[]>
           {
                new object[] { 5, "Wednesday", new List<string> { "1", "2", "Wizz", "4", "Wuzz" } },
                new object[] { 5, "Tuesday", new List<string> { "1", "2", "Fizz", "4", "Buzz" } },
                new object[] { 15, "Tuesday", new List<string> { "1", "2", "Fizz", "4", "Buzz", "6", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" } }
           };

        [Theory]
        [InlineData(30, "Wednesday", "Wizz Wuzz")]
        [InlineData(33, "Wednesday", "Wizz")]
        [InlineData(35, "Wednesday", "Wuzz")]
        [InlineData(30, "Monday", "Fizz Buzz")]
        [InlineData(33, "Monday", "Fizz")]
        [InlineData(35, "Monday", "Buzz")]
        [InlineData(29, "Monday", "29")]
        public void Test_ProcessFizzBuzzDataMethod(int inputValue1, string inputValue2, string expectedValue)
        {
            //Arrange
            var result = new CreateTextService(rules);

            //Act
            var actualResult = result.ProcessFizzBuzzData(inputValue1, inputValue2);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }
    }
}
