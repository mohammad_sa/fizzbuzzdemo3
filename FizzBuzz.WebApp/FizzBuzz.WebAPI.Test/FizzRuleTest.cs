﻿using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.WebAPI.Test
{
    public class FizzRuleTest
    {
        [Fact]
        public void TestFizzRule_IsNumberMatched_WhenNumberDivisibleBy3()
        {
            //Arrange
            var result = new FizzRule();

            //Act
            bool actualResult = result.IsNumberMatched(6);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestFizzRule_IsNumberMatched_WhenNumberNotDivisibleBy3()
        {
            //Arrange
            var result = new FizzRule();

            //Act
            bool actualResult = result.IsNumberMatched(5);

            //Assert
            Assert.False(actualResult);
        }

        [Theory]
        [InlineData("Wednesday", "Wizz")]
        [InlineData("Monday", "Fizz")]
        [InlineData("Tuesday", "Fizz")]
        public void TestFizzRule_GetReplacedWord(string inputValue, string expectedValue)
        {
            //Arrange
            var result = new FizzRule();
            //Act
            var actualResult = result.GetReplacedWord(inputValue);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }

    }
}
