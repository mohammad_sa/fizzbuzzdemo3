﻿using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.WebAPI.Test
{

    public class BuzzRuleTest
    {
        [Fact]
        public void TestBuzzRule_IsNumberMatched_WhenNumberDivisibleBy5()
        {
            //Arrange
            var result = new BuzzRule();

            //Act
            bool actualResult = result.IsNumberMatched(20);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestBuzzRule_IsNumberMatched_WhenNumberNotDivisibleBy5()
        {
            //Arrange
            var result = new BuzzRule();

            //Act
            bool actualResult = result.IsNumberMatched(7);

            //Assert
            Assert.False(actualResult);
        }

        [Theory]
        [InlineData("Wednesday", "Wuzz")]
        [InlineData("Monday", "Buzz")]
        [InlineData("Tuesday", "Buzz")]
        public void TestBuzzRule_GetReplacedWord(string inputValue, string expectedValue)
        {
            //Arrange
            var result = new BuzzRule();
            //Act
            var actualResult = result.GetReplacedWord(inputValue);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }

    }
}
