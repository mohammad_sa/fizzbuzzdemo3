﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzz.WebAPIService.BusinessLayer.CreateText;
using FizzBuzz.WebAPIService.BusinessLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FizzBuzz.WebAPIService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FizzBuzzWebAPIController : ControllerBase
    {
        private readonly ICreateTextService _messages;
        private readonly IDayOfTheWeek _dayOfTheWeek;

        public FizzBuzzWebAPIController(ICreateTextService messages, IDayOfTheWeek dayOfTheWeek)
        {
            _messages = messages;
            _dayOfTheWeek = dayOfTheWeek;
        }

        [HttpGet("{inputNumber}")]
        public IList<string> GetMessages(int inputNumber)
        {
            return _messages.GetMessages(inputNumber, _dayOfTheWeek.GetCurrentDay().ToString());
        }
    }
}
