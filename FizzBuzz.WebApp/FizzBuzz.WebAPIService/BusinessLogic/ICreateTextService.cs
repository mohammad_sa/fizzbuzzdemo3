﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLogic
{
    public interface ICreateTextService
    {
        public IList<string> GetMessages(int inputNumber, string dayOfTheWeek);
    }
}
