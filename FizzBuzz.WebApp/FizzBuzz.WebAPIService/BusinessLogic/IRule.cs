﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLogic
{
    public interface IRule
    {
        bool IsNumberMatched(int inputNumber);
        string GetReplacedWord(string dayOfTheWeek);
    }
}
