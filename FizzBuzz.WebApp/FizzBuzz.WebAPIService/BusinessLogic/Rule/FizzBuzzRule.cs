﻿using FizzBuzz.WebAPIService.BusinessLogic.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLogic.Rule
{
    public class FizzBuzzRule : IRule
    {
        public bool IsNumberMatched(int enteredNumber)

        {
            return enteredNumber % 3 == 0 && enteredNumber % 5 == 0;
        }

        public string GetReplacedWord(string dayOfTheWeek)
        {
            return (dayOfTheWeek == Constants.DayWeekRule) ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}
