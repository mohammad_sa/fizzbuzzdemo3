﻿using FizzBuzz.WebAPIService.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLayer.CreateText
{
    public class CreateTextService : ICreateTextService
    {
        private readonly IEnumerable<IRule> _Rules;

        public CreateTextService(IEnumerable<IRule> Rules)
        {
            _Rules = Rules;
        }
        public IList<string> GetMessages(int inputNumber, string dayOfTheWeek)
        {
            IList<string> messages = new List<string>();

            for (int sequenceNumber = 1; sequenceNumber <= inputNumber; sequenceNumber++)
            {
                messages.Add(ProcessFizzBuzzData(sequenceNumber, dayOfTheWeek));
            }
            return messages;
        }

        public string ProcessFizzBuzzData(int sequenceNumber, string dayOfTheWeek)
        {
            foreach (var rule in _Rules)
            {
                if (rule.IsNumberMatched(sequenceNumber))
                {
                    return rule.GetReplacedWord(dayOfTheWeek);
                }
            }
            return Convert.ToString(sequenceNumber);
        }
    }
}
