using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzz.WebAPIService.BusinessLayer.CreateText;
using FizzBuzz.WebAPIService.BusinessLogic;
using FizzBuzz.WebAPIService.BusinessLogic.DayOfTheWeek;
using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FizzBuzz.WebAPIService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();
            services.AddTransient<ICreateTextService, CreateTextService>();
            services.AddTransient<IRule, FizzRule>();
            services.AddTransient<IRule, BuzzRule>();
            services.AddTransient<IRule, FizzBuzzRule>();
            services.AddTransient<IDayOfTheWeek, DayOfTheWeek>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=FizzBuzzWebAPI}/{action=GetMessages}/{id?}");
            });
        }
    }
}
